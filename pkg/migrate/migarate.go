package migrate

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

// table name is for checking if table exists
// if not exec sql file wit path variable like schema/user.sql
func Migrate(db *sqlx.DB, tableName string, query string) {
	rows, table_check := db.Query("select * from " + tableName + ";")
	if table_check == nil {
		fmt.Println("Table is there")
		rows.Close()

		return
	}
	fmt.Printf("Creating %s table...", tableName)
	RunCommand(db, query)
	fmt.Printf("%s table created", tableName)
}

func RunCommand(db *sqlx.DB, query string) {
	if _, err := db.Exec(string(query)); err != nil {
		panic(err)
	}
}
