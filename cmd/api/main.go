package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
	"github.com/jmoiron/sqlx"
	"gitlab.com/k640/x-user/internal/migrations"
	"gitlab.com/k640/x-user/internal/services"
	"gitlab.com/k640/x-user/internal/storage/mysql"
	"gitlab.com/k640/x-user/internal/transport/rest"
	"gitlab.com/k640/x-user/pkg/db"
)

func main() {
	var dbase *sqlx.DB
	if _, present := os.LookupEnv("DB_NAME"); present {
		dbase = db.NewMySQL(os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), "db:3306", os.Getenv("DB_NAME"))
	} else {
		dbase = db.NewMySQL("user", "user", "127.0.0.1:8001", "user")
	}
	migrations.RunMigrations(dbase)
	userRepo := mysql.NewUser(dbase)
	validate := validator.New()
	userService := services.NewUser(validate, userRepo)
	userHandler := rest.NewUserHandler(userService)
	r := gin.Default()
	r.POST("/register", userHandler.Register)
	r.POST("/get-by-credential", userHandler.GetByCredential)
	r.GET("/email-exists", userHandler.CheckEmail)
	r.GET("/nickname-exists", userHandler.CheckNickname)
	r.GET("/get-by-id", userHandler.GetById)
	r.GET("/get-by-array-id", userHandler.GetByIdArray)
	// r.POST("/upload", func(c *gin.Context) {
	// 	// single file
	// 	file, err := c.FormFile("avatar")
	// 	if err != nil {

	// 	}
	// 	log.Println(file.Filename)

	// 	c.SaveUploadedFile(file, "/uploads/avatars")

	// 	c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
	// })
	if value, present := os.LookupEnv("APP_PORT"); present {
		r.Run(":" + value)
	} else {
		r.Run(":8003")
	}

}
