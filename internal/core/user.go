package core

type UserId uint64

type User struct {
	Id       UserId `json:"id" db:"id"`
	Nickname string `json:"nickname" binding:"required" db:"nickname"`
	// Mask     uint64 `json:"mask" binding:"required"`
	Email    string `json:"email" binding:"required" validate:"email" db:"email"`
	Password string `json:"password" binding:"required" db:"password"`
}

type UserCheckEmail struct {
	Email string `json:"email" binding:"required" validate:"email"`
}

type Credentials struct {
	Email    string `json:"email" binding:"required" validate:"email"`
	Password string `json:"password" binding:"required"`
}
