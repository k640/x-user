package mysql

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/k640/x-user/internal/core"
	"gitlab.com/k640/x-user/internal/migrations"
	"gitlab.com/k640/x-user/pkg/db"
)

func TestStore(t *testing.T) {
	db := db.NewMySQL("user", "user", "127.0.0.1:8000", "user")
	migrations.RunMigrations(db)
	defer migrations.DropTabels(db)
	repo := NewUser(db)

	userId, err := repo.Store(core.User{
		Nickname: "vlad",
		Email:    "kapagaev@kap.a",
		Password: "sdfsdfdsf",
	})
	assert.Nil(t, err)
	assert.Equal(t, core.UserId(1), userId)
	userId, err = repo.Store(core.User{
		Nickname: "vlad2",
		Email:    "kapagaev@kap2.a",
		Password: "sdfsdfdsffd",
	})

	assert.Nil(t, err)
	assert.Equal(t, core.UserId(2), userId)
}

func TestNicknameExists(t *testing.T) {
	db := db.NewMySQL("user", "user", "127.0.0.1:8000", "user")
	migrations.RunMigrations(db)
	defer migrations.DropTabels(db)
	repo := NewUser(db)
	repo.Store(core.User{
		Nickname: "vlad",
		Email:    "kapagaev@kap.a",
		Password: "sdfsdfdsf",
	})
	result, _ := repo.NicknameExists("vlad")
	assert.True(t, result)
	result, _ = repo.NicknameExists("vlad1")
	assert.False(t, result)
}

func TestEmailExists(t *testing.T) {
	db := db.NewMySQL("user", "user", "127.0.0.1:8000", "user")
	migrations.RunMigrations(db)
	defer migrations.DropTabels(db)
	repo := NewUser(db)
	repo.Store(core.User{
		Nickname: "vlad",
		Email:    "kapagaev@kap.a",
		Password: "sdfsdfdsf",
	})
	result, _ := repo.EmailExists("kapagaev@kap.a")
	assert.True(t, result)
	result, _ = repo.EmailExists("s@s.s")
	assert.False(t, result)
}

func TestGetByEmail(t *testing.T) {
	db := db.NewMySQL("user", "user", "127.0.0.1:8000", "user")
	migrations.RunMigrations(db)
	defer migrations.DropTabels(db)
	repo := NewUser(db)
	testUser := core.User{
		Nickname: "vlad",
		Email:    "kapagaev@kap.a",
		Password: "sdfsdfdsf",
	}
	repo.Store(testUser)
	result, _ := repo.GetByEmail("kapagaev@kap.a")

	assert.Equal(t, testUser.Nickname, result.Nickname)
	result, _ = repo.GetByEmail("s@s.s")
	assert.Nil(t, result)
}

func TestGetById(t *testing.T) {
	db := db.NewMySQL("user", "user", "127.0.0.1:8000", "user")
	migrations.RunMigrations(db)
	defer migrations.DropTabels(db)
	repo := NewUser(db)
	testUser := core.User{
		Nickname: "vlad",
		Email:    "kapagaev@kap.a",
		Password: "sdfsdfdsf",
	}
	userId, _ := repo.Store(testUser)
	result, _ := repo.GetById(userId)
	fmt.Print(result)
	assert.Equal(t, testUser.Nickname, result.Nickname)
	result, _ = repo.GetById(2)
	assert.Nil(t, result)
}
