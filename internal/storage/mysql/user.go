package mysql

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/k640/x-user/internal/core"
)

type User struct {
	db *sqlx.DB
}

func (u *User) NicknameExists(nickname string) (bool, error) {
	sqlStmt := `SELECT nickname FROM user WHERE nickname = ?`
	err := u.db.QueryRow(sqlStmt, nickname).Scan(&nickname)
	if err != nil {
		if err != sql.ErrNoRows {
			// a real error happened! you should change your function return
			// to "(bool, error)" and return "false, err" here
			return false, err
		}

		return false, nil
	}

	return true, nil
}

func (u *User) GetById(id core.UserId) (*core.User, error) {
	var user core.User
	sqlStmt := `SELECT * FROM user WHERE id = ?`
	err := u.db.QueryRowx(sqlStmt, id).StructScan(&user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func (u *User) EmailExists(email string) (bool, error) {
	sqlStmt := `SELECT email FROM user WHERE email = ?`
	err := u.db.QueryRow(sqlStmt, email).Scan(&email)
	if err != nil {
		if err != sql.ErrNoRows {
			// a real error happened! you should change your function return
			// to "(bool, error)" and return "false, err" here
			return false, err
		}

		return false, nil
	}

	return true, nil
}

func NewUser(db *sqlx.DB) *User {
	return &User{db}
}

func (u *User) Store(user core.User) (core.UserId, error) {
	result, err := u.db.Exec("INSERT INTO `user` (`nickname`, `email`, `password`) VALUES (?, ?, ?)", user.Nickname, user.Email, user.Password)
	if err != nil {
		return 0, fmt.Errorf("StoreUser: %v", err)
	}
	id, err := result.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("StoreUser: %v", err)
	}
	return core.UserId(id), nil
}

func (u *User) GetByEmail(email string) (*core.User, error) {
	var user core.User
	sqlStmt := `SELECT * FROM user WHERE email = ?`
	err := u.db.QueryRowx(sqlStmt, email).StructScan(&user)
	if err != nil {
		return nil, err
	}
	return &user, nil
}
