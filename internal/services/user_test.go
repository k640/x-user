package services

type CheckEmailData struct {
	Email   string
	Exists  bool
	IsError bool
}

// func TestCheckEmailExists(t *testing.T) {
// 	testData := []CheckEmailData{
// 		{"check@check.com", true, false},
// 		{"checkcheckcom", false, true},
// 		{"check@checkcom", false, true},
// 		{"checkcheck.com", false, true},
// 		{"checkchec@kcom.", false, true},
// 	}
// 	for _, data := range testData {
// 		ctrl := gomock.NewController(t)
// 		defer ctrl.Finish()

// 		validate := mock_services.NewMockValidator(ctrl)
// 		mockRepo := mock_services.NewMockUserRepository(ctrl)
// 		mockRepo.
// 			EXPECT().
// 			EmailExists(func(_ core.UserCheckEmail) (bool, error) {
// 				if data.IsError {
// 					return false, new(error)
// 				}
// 			})
// 		mockRepo.On("EmailExists").Return(data.Exists, nil)
// 		testService := NewUser(validate, mockRepo)
// 		exists, err := testService.CheckEmailExists(core.UserCheckEmail{Email: data.Email})
// 		if data.IsError {
// 			assert.NotNil(t, err)
// 		} else {
// 			assert.Nil(t, err)
// 		}
// 		assert.Equal(t, exists, data.Exists)
// 	}

// }

// type CheckNicknameData struct {
// 	Nickname string
// 	Exists   bool
// 	IsError  bool
// }

// func TestCheckNicknameExists(t *testing.T) {
// 	testData := []CheckNicknameData{
// 		{"check@check.com", true, false},
// 		{"checkcheckcom", false, true},
// 		{"check@checkcom", false, true},
// 		{"checkcheck.com", false, true},
// 		{"checkchec@kcom.", false, true},
// 	}
// 	for _, data := range testData {
// 		validate := validator.New()
// 		mockRepo := new(MockUserRepository)
// 		mockRepo.On("NicknameExists").Return(data.Exists, nil)
// 		testService := NewUser(validate, mockRepo)
// 		exists, err := testService.CheckNicknameExists(data.Nickname)
// 		if data.IsError {
// 			assert.NotNil(t, err)
// 		} else {
// 			assert.Nil(t, err)
// 		}
// 		assert.Equal(t, exists, data.Exists)
// 	}
// }
