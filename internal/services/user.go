package services

import (
	"fmt"

	"gitlab.com/k640/x-user/internal/core"
	"golang.org/x/crypto/bcrypt"
)

//go:generate mockgen -source=user.go -destination=mocks/mock.go
type Validator interface {
	Struct(s interface{}) error
}

//go:generate mockgen -source=user.go -destination=mocks/mock.go
type UserRepository interface {
	Store(core.User) (core.UserId, error)
	EmailExists(string) (bool, error)
	NicknameExists(string) (bool, error)
	GetById(core.UserId) (*core.User, error)
	GetByEmail(email string) (*core.User, error)
}

type User struct {
	repo     UserRepository
	validate Validator
}

func NewUser(validate Validator, repo UserRepository) *User {
	return &User{repo, validate}
}

func (u *User) Create(user core.User) (core.UserId, error) {
	if err := u.validate.Struct(user); err != nil {
		return 0, err
	}
	hash, _ := hashPassword(user.Password)
	user.Password = hash
	userId, err := u.repo.Store(user)
	return userId, err
}

func (u *User) VerifyCredentials(credentials core.Credentials) (core.UserId, error) {
	if err := u.validate.Struct(credentials); err != nil {
		return 0, err
	}
	user, err := u.repo.GetByEmail(credentials.Email)
	if user == nil {
		return 0, fmt.Errorf("invalid credentials")
	}
	if err != nil {
		return 0, err
	}
	if !checkPasswordHash(credentials.Password, user.Password) {
		return 0, fmt.Errorf("invalid credentials")
	}
	return user.Id, err
}

func (u *User) CheckEmailExists(email core.UserCheckEmail) (bool, error) {
	if err := u.validate.Struct(email); err != nil {
		return false, err
	}
	exists, err := u.repo.EmailExists(email.Email)
	return exists, err
}

func (u *User) CheckNicknameExists(nickname string) (bool, error) {
	exists, err := u.repo.NicknameExists(nickname)
	return exists, err
}

func (u *User) GetById(id core.UserId) (*core.User, error) {
	user, err := u.repo.GetById(id)
	return user, err
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}
