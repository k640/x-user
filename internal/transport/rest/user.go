package rest

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/k640/x-user/internal/core"
)

//go:generate mockgen -source=user.go -destination=mocks/mock.go
type UserService interface {
	Create(core.User) (core.UserId, error)
	CheckEmailExists(email core.UserCheckEmail) (bool, error)
	CheckNicknameExists(nickname string) (bool, error)
	GetById(core.UserId) (*core.User, error)
	VerifyCredentials(core.Credentials) (core.UserId, error)
}

type UserHandler struct {
	service UserService
}

func NewUserHandler(userService UserService) *UserHandler {
	return &UserHandler{userService}
}

func (u *UserHandler) Register(c *gin.Context) {
	var user core.User
	if err := c.Bind(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	userId, err := u.service.Create(user)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"user_id": userId,
	})
}

func (u *UserHandler) GetByCredential(c *gin.Context) {
	var credentials core.Credentials
	if err := c.Bind(&credentials); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	userId, err := u.service.VerifyCredentials(credentials)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"user_id": userId,
	})
}

func (u *UserHandler) GetById(c *gin.Context) {
	userId, err := strconv.Atoi(c.Query("user_id"))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "invalid user_id"})
		return
	}
	user, err := u.service.GetById(core.UserId(userId))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "user not found"})
		return
	}
	c.JSON(http.StatusOK, user)
}

func (u *UserHandler) CheckEmail(c *gin.Context) {
	email := c.Query("email")
	exists, err := u.service.CheckEmailExists(core.UserCheckEmail{Email: email})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"exists": exists,
	})
}

//check nickname
func (u *UserHandler) CheckNickname(c *gin.Context) {
	nickname := c.Query("nickname")
	if nickname == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "nickname is empty"})
		return
	}
	exists, err := u.service.CheckNicknameExists(nickname)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"exists": exists,
	})
}

type getUsersByIdArray struct {
	Ids []core.UserId `json:"users"`
}

func (u *UserHandler) GetByIdArray(c *gin.Context) {
	var ids getUsersByIdArray
	if err := c.BindJSON(&ids); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	var users []*core.User
	for _, id := range ids.Ids {
		user, err := u.service.GetById(id)
		fmt.Println(user)
		if err != nil {
			continue
		}
		users = append(users, user)
	}
	c.JSON(http.StatusOK, users)
}

// func (u *UserHandler) Update(c *gin.Context) {
// 	var user core.User
// 	if err := c.Bind(&user); err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
// 		return
// 	}
// 	userId, err := u.service.Update(user)
// 	if err != nil {
// 		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
// 		return
// 	}
// 	c.JSON(http.StatusOK, gin.H{
// 		"user_id": userId,
// 	})
// }
