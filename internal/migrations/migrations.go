package migrations

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/k640/x-user/pkg/migrate"
)

func RunMigrations(db *sqlx.DB) {
	migrate.Migrate(db, "user", user)
}

const user string = `
CREATE TABLE ` + `user` + ` (
	id         INT AUTO_INCREMENT NOT NULL,
	nickname      VARCHAR(32) NOT NULL UNIQUE,
	email   VARCHAR(255) NOT NULL UNIQUE,
	password  VARCHAR(255) NOT NULL,
	PRIMARY KEY (` + `id` + `)
  );
`

const user_drop string = "DROP TABLE `user`;"

func DropTabels(db *sqlx.DB) {
	migrate.RunCommand(db, user_drop)

}
